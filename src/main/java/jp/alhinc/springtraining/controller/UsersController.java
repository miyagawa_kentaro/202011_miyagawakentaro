package jp.alhinc.springtraining.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.Department;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.SettingsForm;
import jp.alhinc.springtraining.service.BranchService;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.DepartmentService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.IsStoppedService;
import jp.alhinc.springtraining.service.SettingsService;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private BranchService branchService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private SettingsService settingsService;

	@Autowired
	private IsStoppedService isStoppedService;

	public static final String STR_USERS = "users";
	public static final String STR_BRANCHES = "branches";
	public static final String STR_DEPARTMENTS = "departments";

	@GetMapping
	public String index(Model model) {

		List<User> users = getAllUsersService.getAllUsers();
		List<Branch> branches = branchService.getBranchList();
		List<Department> departments = departmentService.getDepartmentList();
		model.addAttribute(STR_USERS, users);
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENTS, departments);
		System.out.println(users);
		return "users/index";
	}

	@GetMapping("/create")
	public String create(Model model) {

		List<Branch> branches = branchService.getBranchList();
		List<Department> departments = departmentService.getDepartmentList();
		model.addAttribute("form", new CreateUserForm());
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENTS, departments);
		return "users/create";
	}

	@PostMapping("/create")
	public String create(@ModelAttribute("form") @Validated CreateUserForm form,
			BindingResult result, Model model) {

		List<Branch> branches = branchService.getBranchList();
		List<Department> departments = departmentService.getDepartmentList();

		if (result.hasErrors()) {
			model.addAttribute("message", "残念");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENTS, departments);
			return "users/create";
		}
		boolean isEmpty =  createUserService.comfirmLoginId(form.getLoginId());
			if (isEmpty == true) {
				createUserService.create(form);
				return "redirect:/users";
			} else {
				model.addAttribute("message", "そのLOGIN＿IDはすでに使われています");
				model.addAttribute(STR_BRANCHES, branches);
				model.addAttribute(STR_DEPARTMENTS, departments);
				return "users/create";
			}
	}

	@GetMapping("/settings")
	public String settings(@RequestParam int id, Model model) {

		List<Branch> branches = branchService.getBranchList();
		List<Department> departments = departmentService.getDepartmentList();
		SettingsForm form = settingsService.getSettingsUser(id);
		model.addAttribute("settingsForm", form);
		model.addAttribute(STR_BRANCHES, branches);
		model.addAttribute(STR_DEPARTMENTS, departments);
		return "users/settings";
	}

	@PostMapping("/settings")
	public String updateUser(@ModelAttribute("settingsForm") SettingsForm form,
			BindingResult result, Model model) {

		List<Branch> branches = branchService.getBranchList();
		List<Department> departments = departmentService.getDepartmentList();
		if (result.hasErrors()) {
			model.addAttribute("message", "残念");
			model.addAttribute(STR_BRANCHES, branches);
			model.addAttribute(STR_DEPARTMENTS, departments);
			return "users/settings";
		}
		boolean isEmpty =  settingsService.comfirmLoginId(form.getLoginId(),form.getId());
			if (isEmpty == true) {
		settingsService.updateUser(form);
		return "redirect:/users";
			} else {
				model.addAttribute("message", "そのLOGIN＿IDはすでに使われています");
				model.addAttribute(STR_BRANCHES, branches);
				model.addAttribute(STR_DEPARTMENTS, departments);
				return "users/settings";
			}
	}

	@PostMapping("/stop")
	public String stopUser(SettingsForm form) {

		isStoppedService.stopUser(form);
		return "redirect:/users";
	}

	@PostMapping("/start")
	public String startUser(SettingsForm form) {

		isStoppedService.startUser(form);
		return "redirect:/users";
	}

}

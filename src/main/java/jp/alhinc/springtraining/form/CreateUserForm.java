package jp.alhinc.springtraining.form;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class CreateUserForm implements Serializable {

	@NotBlank(message = "名前必須項目です")
	@Size(max = 10,message = "名前10文字以下で入力して下さい")
	public String name;

	@Size(min = 6, max = 20,message = "password6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9 -~]*$", message = "password記号を含む全ての半角文字のみ有効です")
	public String rawPassword;

	@NotBlank(message = "確認用パスワードを入力してください")
	public String comfirmPw;

	@AssertTrue(message = "確認用パスワードエラーです")
	public boolean isPasswordValid() {
		if (rawPassword.equals(comfirmPw)) return true;
		return false;
	}

	@NotBlank(message = "ログインID必須項目です")
	@Size(min = 6, max = 20,message = "ログインID6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message = "ログインID半角英数字のみ有効です")
	public String loginId;

	public int branchId;
	public int departmentId;

}

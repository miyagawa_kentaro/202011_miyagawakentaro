package jp.alhinc.springtraining.form;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class SettingsForm {

	public int id;

	@NotBlank(message = "名前必須項目です")
	@Size(max = 10,message = "名前10文字以下で入力して下さい")
	public String name;

	@AssertTrue(message = "password記号を含む全ての半角文字6文字から20文字で入力して下さい")
	public boolean passwordBlankValid() {
		if (rawPassword != null || (rawPassword.matches("^[a-zA-Z0-9 -~]*$")
				&& rawPassword.length() >= 6
				&& rawPassword.length() <= 20)) {
			return true;
		}
		return false;
	}

	public String rawPassword;

	@AssertTrue(message = "確認用パスワードを入力してください")
	public boolean isComfirmValid() {
		if (rawPassword != null && comfirmPassword != null) {
			return true;
		}
		return false;
	}

	public String comfirmPassword;

	@AssertTrue(message = "パスワードと確認用パスワードが一致しません")
	public boolean isPasswordValid() {
		if (rawPassword == null || rawPassword.isEmpty())
			return true;
		return rawPassword.equals(comfirmPassword);
	}

	@NotBlank(message = "ログインID必須項目です")
	@Size(min = 6, max = 20,message = "ログインID6文字から20文字で入力して下さい")
	@Pattern(regexp = "^[a-zA-Z0-9]{6,20}", message = "ログインID半角英数字のみ有効です")
	public String loginId;

	public int branchId;
	public int departmentId;
	public int isStopped;

}

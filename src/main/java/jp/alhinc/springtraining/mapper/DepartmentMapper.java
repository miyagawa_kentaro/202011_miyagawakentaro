package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.Department;

@Mapper
public interface DepartmentMapper {

	List<Department> departmentList();

}

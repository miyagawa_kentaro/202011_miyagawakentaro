package jp.alhinc.springtraining.entity;

import lombok.Data;

@Data
public class User {

	private int id;

	private String name;

	private String password;

	private String loginId;

	private int branchId;

	private String branchName;

	private int departmentId;

	private String departmentName;

	private int is_stopped;

}

package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.mapper.BranchMapper;

@Service
public class BranchService {

	@Autowired
	private BranchMapper mapper;

	@Transactional
	public List<Branch> getBranchList() {
		return mapper.branchList();
	}

}

package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.SettingsForm;
import jp.alhinc.springtraining.mapper.UserMapper;
@Service
public class IsStoppedService {

	@Autowired
	private UserMapper mapper;

	public void stopUser(SettingsForm form) {

		User entity = new User();
		entity.setId(form.getId());
		mapper.isStop(entity);
	}

	public void startUser(SettingsForm form) {

		User entity = new User();
		entity.setId(form.getId());
		mapper.isStart(entity);
	}

}

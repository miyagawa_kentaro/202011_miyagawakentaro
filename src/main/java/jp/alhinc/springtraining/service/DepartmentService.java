package jp.alhinc.springtraining.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.Department;
import jp.alhinc.springtraining.mapper.DepartmentMapper;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentMapper mapper;

	@Transactional
	public List<Department> getDepartmentList() {
		return mapper.departmentList();
	}

}

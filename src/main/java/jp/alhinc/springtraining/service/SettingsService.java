package jp.alhinc.springtraining.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.SettingsForm;
import jp.alhinc.springtraining.mapper.UserMapper;
@Service
public class SettingsService {

	@Autowired
	private UserMapper mapper;

	public SettingsForm getSettingsUser(int id){

		User settingsUser = mapper.getUser(id);
		SettingsForm form = new SettingsForm();
		BeanUtils.copyProperties(settingsUser, form);
		form.setRawPassword(settingsUser.getPassword());
		return form;
	}

	public int updateUser(SettingsForm form) {

		User entity = new User();
		BeanUtils.copyProperties(form, entity);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRawPassword()));

		return mapper.updateUser(entity);
	}

	public boolean comfirmLoginId(String loginId, int id) {

		User userLoginId = mapper.getUserLoginId(id);
		User newLoginId = new User();
		User entity = mapper.getLoginId(loginId);
		newLoginId.setLoginId(loginId);
		if (userLoginId.getLoginId().equals(newLoginId.getLoginId())) {
			return true;
		} else if(entity == null){
			return true;
		} else {
			return false;
		}
	}
}
